# BLUEsat Altium Schematic Libraries and footprints

The most common components you'll find under '\_GenericPassives.Schlib' and '\_GenericPassives.PcbLib'.

If you find any other common components feel free to add them, otherwise keep the libraries specific to projects in their respective repositories`
